"""ActiveCampaign target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th

from target_activecampaign.sinks import (
    ActiveCampaignSink,
)


class TargetActiveCampaign(Target):
    """Sample target for ActiveCampaign."""

    name = "target-activecampaign"
    config_jsonschema = th.PropertiesList(
        th.Property("api_url", th.StringType, required=True),
        th.Property("api_token", th.StringType, required=True),
        th.Property("list_id", th.StringType, required=True),
        th.Property("api_version", th.NumberType),
    ).to_dict()
    default_sink_class = ActiveCampaignSink


if __name__ == "__main__":
    TargetActiveCampaign.cli()

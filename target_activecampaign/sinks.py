"""ActiveCampaign target sink class, which handles writing streams."""


from singer_sdk.sinks import BatchSink
import requests


class ActiveCampaignSink(BatchSink):
    """ActiveCampaign target sink class."""

    contacts = []

    @property
    def url_base(self):
        url = self.config.get("api_url")
        api_version = int(self.config.get("api_version", 3))
        return f"{url}/api/{api_version}"

    @property
    def http_headers(self):
        headers = {}
        headers["Content-Type"] = "application/json"
        headers["Api-Token"] = self.config.get("api_token")
        return headers

    def add_to_list(self, contact_id):
        contact = self.get_contact(contact_id)
        if "contact" in contact:
            payload = {
                "contactList": {
                    "list": int(self.config.get("list_id")),
                    "contact": contact["contact"]["id"],
                    "status": 1,
                }
            }
            url = url = f"{self.url_base}/contactLists"
            resp = requests.post(url, headers=self.http_headers, json=payload)

    def process_record(self, record: dict, context: dict) -> None:

        if self.stream_name.lower() in ["contacts", "contact", "customer", "customers"]:  
            
            mapping = {
                "email": record.get("email"),
                "firstName": record.get("first_name"),
                "lastName": record.get("last_name"),
                "first_name": record.get("first_name"),
                "last_name": record.get("last_name"),
                "custom_fields": [],
            }
            if record.get("phone_numbers"):
                mapping["phone"] = record.get("phone_numbers")[0].get("number")
            if record.get("phone"):
                mapping["phone"] = record.get("phone")
            if record.get("custom_fields"):
                for field in record.get("custom_fields"):
                    mapping["custom_fields"].append(
                        {
                            "field": field.get("name"),
                            "value": field.get("value"),
                        }
                    )
            if record.get("subscribe_status") == "subscribed" or record.get("subscribe_status") is True:
                mapping["subscribe"] = [{"listid": self.config.get("list_id")}]
            else:
                mapping["subscribe"] = False

            self.contacts.append(mapping)

        if self.stream_name == "AddToList":
            self.add_to_list(record["id"])

    def get_contact(self, contact_id):
        url = url = f"{self.url_base}/contacts/{contact_id}"
        contact = requests.get(url, headers=self.http_headers)
        return contact.json()

    def unsubscribe_contact(self,contact_id,contact,status=2):
        payload = {
                "contactList": {
                    "list": int(self.config.get("list_id")),
                    "contact": contact_id,
                    "status": status, #Unsubscribed
                }
            }
        subscribe_text = "unsubscribed"
        if status==1:
            subscribe_text = "subscribed"

        url =  f"{self.url_base}/contactLists"
        response = requests.post(url, headers=self.http_headers, json=payload)
        response.raise_for_status()
        if response.status_code == 200:
            print(f"Contact {contact.get('firstName')} {contact.get('firstName')} : {contact_id} {subscribe_text} successfully")
            


    def process_batch(self, context: dict) -> None:
        if self.stream_name.lower() in ["contacts", "contact", "customer", "customers"]:
            if len(self.contacts)>0:
                #Check if there are contacts to update
                for contact in self.contacts:
                    #Check if contact exists by email
                    if contact.get("email"):
                        url = f"{self.url_base}/contacts"
                        payload = {"email": contact.get("email")}
                        response = requests.get(url, params=payload, headers=self.http_headers)
                        response.raise_for_status()
                        if response.status_code == 200:
                            #Contact exists, update
                            contacts_active_campaign = response.json().get("contacts",[])
                            if len(contacts_active_campaign)>0:
                                contact_id = contacts_active_campaign[0]["id"]
                                url = f"{self.url_base}/contacts/{contact_id}"
                                response = requests.put(url, json={"contact":contact}, headers=self.http_headers)
                                response.raise_for_status()
                                if response.status_code == 200:
                                    print(f"Contact {contact.get('firstName')} {contact.get('firstName')}: {contact_id} updated successfully")
                                    #Check if contact needs to be unsubscribed
                                    if contact.get("subscribe") is False:
                                        self.unsubscribe_contact(contact_id,contact)
                                    else:
                                        #Since we don't have subscription detail in contact detail object. To facilitate list subscription change make a patch request for list as well.
                                        self.unsubscribe_contact(contact_id,contact,1)   
                                    #pop contact from list
                                    self.contacts.pop(self.contacts.index(contact))
                if len(self.contacts) > 0:
                    #Import remaining contacts                    
                    payload = {"contacts": self.contacts, "callback": None}
                    url = f"{self.url_base}/import/bulk_import"
                    response = requests.post(url, json=payload, headers=self.http_headers)
                    response.raise_for_status()
                    if response.status_code == 200:
                        print("Contacts imported successfully")
            